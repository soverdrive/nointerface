package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/soverdrive/nointerface/database/sqlite"
	"gitlab.com/soverdrive/nointerface/handler"
	"gitlab.com/soverdrive/nointerface/user"
)

func main() {
	log.Printf("check go")

	if err := sqlite.Init(); err != nil {
		log.Fatalf("failed to initialise sqlite %v", err)
	}

	nointerfaceRouter := chi.NewRouter()
	nointerfaceRouter.Get("/health", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(fmt.Sprintf("{\"data\":\"OK\"}")))
	})

	// user section:start
	if err := user.Init(); err != nil {
		log.Fatalf("failed to initialise user %v", err)
	}

	nointerfaceRouter.Post("/user", handler.UserCreate)
	nointerfaceRouter.Get("/user", handler.UserGet)
	// user section:end

	log.Printf("waiting for request at :9000...")
	http.ListenAndServe(":9000", nointerfaceRouter)
}
