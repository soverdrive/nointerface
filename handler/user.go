package handler

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/soverdrive/nointerface/user"
)

type userCreateDataParser struct {
	Name string `json:"name"`
}

func UserCreate(w http.ResponseWriter, r *http.Request) {
	rawPayload, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("{\"error\":\"malformed body %v\"}", err)))
		return
	}

	var payload userCreateDataParser
	if err := json.Unmarshal(rawPayload, &payload); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("{\"error\":\"invalid json %v\"}", err)))
		return
	}

	userData, err := user.CreateUser(r.Context(), user.CreateUserParam{Name: payload.Name})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("{\"error\":\"failed to create user %v\"}", err)))
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(fmt.Sprintf("{\"data\":\"%d\"}", userData.ID)))
}

type getUserDataParser struct {
	IDString string
	ID       int64
}

func (gu *getUserDataParser) Normalise() (err error) {
	if gu.IDString == "" {
		return
	}

	gu.ID, err = strconv.ParseInt(gu.IDString, 10, 64)
	return
}

type getUserRespParser struct {
	Data user.GetUserResult `json:"data"`
}

func UserGet(w http.ResponseWriter, r *http.Request) {
	var param getUserDataParser
	param.IDString = r.URL.Query().Get("id")

	if err := param.Normalise(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("{\"error\":\"param invalid %v\"}", err)))
		return
	}

	userResult, err := user.GetUser(r.Context(), user.GetUserParam{UserID: param.ID})
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(fmt.Sprintf("{\"error\":\"cannot find resource %v\"}", err)))
		return
	} else if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("{\"error\":\"failed to get user %v\"}", err)))
		return
	}

	marshaledResult, err := json.Marshal(getUserRespParser{Data: userResult})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("{\"error\":\"malformed response %v\"}", err)))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(marshaledResult)
}
