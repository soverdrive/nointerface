package sqlite

import (
	"database/sql"

	_ "github.com/glebarez/go-sqlite"
)

var sqlite *sql.DB

func Init() (err error) {
	if sqlite != nil {
		return
	}

	sqlite, err = sql.Open("sqlite", "./nointerface.db")
	return
}

func Retrieve() *sql.DB {
	return sqlite
}
