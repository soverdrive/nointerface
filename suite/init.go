package suite

import "fmt"

type CrudFunc func(param, data interface{}) error

type testSuite struct {
	crud map[string][]CrudFunc
}

var suiteTest *testSuite

const (
	suiteGet int = iota
	suiteCreate
)

func Init() error {
	if suiteTest != nil {
		return nil
	}

	suiteTest = &testSuite{
		crud: make(map[string][]CrudFunc),
	}
	return nil
}

func RegisterCreate(category string, crudFn CrudFunc) error {
	return suiteTest.register(category, suiteCreate, crudFn)
}

func RegisterGet(category string, crudFn CrudFunc) error {
	return suiteTest.register(category, suiteGet, crudFn)
}

func (st *testSuite) register(category string, suiteIdx int, crudFn CrudFunc) error {
	collection, ok := st.crud[category]
	if !ok {
		st.crud[category] = make([]CrudFunc, 2)
	}

	currentFn := collection[suiteIdx]
	if currentFn != nil {
		return fmt.Errorf("duplicate")
	}

	return nil
}
