package user

import (
	"context"
	"fmt"

	"gitlab.com/soverdrive/nointerface/suite"
)

// CHECKPOINT
//
// untuk menjadi server faker, test suite tidak bisa bertindak sebagai
// pengganti fungsi seperti mock.
// test suite harus menggantikan event/flow yang bisa terjadi sesuai dengan
// kebutuhan, e.g.
//
//	event: create user
//	func: eksekusi semua method yang dibutuhkan untuk membuat user
//
//	event: get user
//	func: eksekusi semua method yang dibutuhkan untuk mengambil user
//
// hasil dari event faker tidak bisa diharapkan sebagai struktur data
// paling dasar yang didapat, e.g. struct sesuai dengan hasil query.
// hasil yang bisa diharapkan adalah hasil akhir dari event/flow
// terkait, e.g.
//
//	event get user akan mengembalikan struktur data yang dipakai untuk
//	merespon API GET user

// CHECKPOINT
// alternatif dari layer repository adalah membuat file khusus untuk akses
// service lain seperti database, cache, atau API lain, e.g.
//
//	./user/
//		init.go
//		suite.go
//		database.go
//		redis.go
//	 	qrcode.go
func registerSuite() {
	suite.RegisterCreate("user", func(param, data interface{}) error {
		casted, ok := param.(CreateUserParam)
		if param != nil && !ok {
			return fmt.Errorf("param must be CreateUserParam")
		}

		userCreated, err := CreateUser(context.Background(), casted)
		if err != nil {
			return err
		}

		if data == nil {
			return nil
		}

		_, ok = data.(CreateUserData)
		if !ok {
			return fmt.Errorf("data must be CreateUserData")
		}

		data = &userCreated

		return nil
	})

	suite.RegisterGet("user", func(param, data interface{}) error {
		casted, ok := param.(GetUserParam)
		if !ok {
			return fmt.Errorf("param must be GetUserParam")
		}

		userCreated, err := GetUser(context.Background(), casted)
		if err != nil {
			return err
		}

		_, ok = data.(CreateUserData)
		if !ok {
			return fmt.Errorf("data must be CreateUserData")
		}

		data = &userCreated

		return nil
	})
}
