package user

import (
	"context"
	"database/sql"

	"gitlab.com/soverdrive/nointerface/database/sqlite"
)

type userControl struct {
}

var userControlData *userControl

func Init() (err error) {
	if userControlData != nil {
		return
	}

	userControlData = &userControl{}

	registerSuite()
	return
}

type CreateUserParam struct {
	Name string
}

type CreateUserData struct {
	ID   int64
	Name string
}

var queryMasterCreateUser = `INSERT INTO user (name) VALUES ($1) RETURNING id, name`

func CreateUser(ctx context.Context, param CreateUserParam) (createUser CreateUserData, err error) {
	err = sqlite.Retrieve().QueryRowContext(ctx, queryMasterCreateUser, param.Name).
		Scan(&createUser.ID, &createUser.Name)
	return
}

type GetUserParam struct {
	UserID int64
}

type GetUserData struct {
	Name string `json:"name"`
}

type GetUserResult struct {
	User  []GetUserData `json:"user"`
	Total int           `json:"total"`
}

var querySlaveGetUser = `SELECT name FROM user WHERE id = $1`
var querySlaveGetMultipleUser = `SELECT name FROM user ORDER BY id DESC`

func GetUser(ctx context.Context, param GetUserParam) (GetUserResult, error) {
	var rowsResult *sql.Rows
	var err error
	if param.UserID != 0 {
		rowsResult, err = sqlite.Retrieve().QueryContext(ctx, querySlaveGetUser, param.UserID)
	} else {
		rowsResult, err = sqlite.Retrieve().QueryContext(ctx, querySlaveGetMultipleUser)
	}

	var result []GetUserData
	for rowsResult.Next() {
		var oneUser GetUserData
		err = rowsResult.Scan(&oneUser.Name)
		if err != nil {
			return GetUserResult{}, err
		}

		result = append(result, oneUser)
	}

	if len(result) == 0 {
		return GetUserResult{}, sql.ErrNoRows
	}

	return GetUserResult{
		User:  result,
		Total: len(result),
	}, nil
}
